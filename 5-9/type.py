a = 1
print(type(a))
# <class 'int'> --整型b = 1.1
b = 1.1
print(type(b))
# <class 'float'> --浮点型c =True
c = True
print(type(c))
# <class 'bool'> --布尔型d = '12345'
d = '12345'
print(type(d))
# <class 'str'> --字符串e = [10，20，30]
e = [10, 20, 30]
print(type(e))
# <class 'list'> --列表f =(10，20，30)
f = (10, 20, 30)
print(type(f))
# <class 'tuple'> --元组h = {10，20，30}
h = {10, 20, 30}
print(type(h))
# <class 'set'> --集合g = { ' name ' :'TOM' , 'age ': 20}
g = {' name ': 'TOM', 'age ': 20}
print(type(g))
# <class 'dict'> --字典
